/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.yaroslav_555.practice;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Ярослав
 */
public class GeneratorHemmingCode {

    public static final int length = 7;
    
    int[] paritybits = {0, 1, 3}; 
    
    int[][] sum = {{2, 4, 6}, {2, 5, 6}, {4, 5, 6}};
    
    public static HashMap<Integer, Formula> formuls = new HashMap<>();
    
    public static void preInit(){
        formuls.put(0, new Formula(0, 1, 3));
        formuls.put(1, new Formula(0, 2, 3));
        formuls.put(3, new Formula(1, 2, 3));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        preInit();
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Hello. You need to put together the third, fifth, sixth and seventh bits.");
        System.out.println("");
        String bytes = sc.nextLine();
        
        int[] array = new int[4];
        
        int length = bytes.length();
        
        for(int i = 0; i<length; i++){
            if(bytes.toCharArray()[i] == '1') array[i] = 1;
            else array[i] = 0;
        }
        
        array = generateHemmingCode(array);
            
        for(int i:array)
            System.out.print(i);
    
        System.out.println();
    }
    
    public static int[] generateHemmingCode(int[] array){
        int[] finalVariable = new int[GeneratorHemmingCode.length];
        
        for(int i = 0, counter = 0; i<GeneratorHemmingCode.length; i++){
            Formula formula = formuls.get(i);
            if(formula == null){
                finalVariable[i] = array[counter++];
                continue;
            }
            int preSum = 0;
            for(int j = 0; j<formula.bytes.length; j++) 
                preSum += array[formula.bytes[j]];
            finalVariable[i] = preSum % 2;
        }
        
        return finalVariable;
    }
    
    
    private static class Formula{
        int[] bytes;

        public Formula(int... bytes) {
            this.bytes = bytes;
        }
        
        
    }
}
